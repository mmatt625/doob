## The Most Up-to-Date version of this repository is avaliable on GitHub https://github.com/mmatt625/doob
# Doob Bot 
[![Discord Bots](https://top.gg/api/widget/status/680606346952966177.svg?noavatar=true)](https://top.gg/bot/680606346952966177)
[![Discord Bots](https://top.gg/api/widget/upvotes/680606346952966177.svg?noavatar=true)](https://top.gg/bot/680606346952966177)
[![Discord Bots](https://top.gg/api/widget/lib/680606346952966177.svg?noavatar=true)](https://top.gg/bot/680606346952966177)
[![Discord Bots](https://discordbots.org/api/widget/owner/680606346952966177.svg?noavatar=true)](https:/top.gg/bot/680606346952966177)
[![GitHub license](https://img.shields.io/github/license/mmatt625/doob.svg)](https://github.com/mmatt625/doob/blob/master/LICENSE)
[![StackShare](http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat)](https://stackshare.io/mmatt/doob)
[![mmatt's Discord](https://discordapp.com/api/guilds/560262402659057681/widget.png?style=shield)](https://discord.gg/8xMWb7W)
<a frameborder="0" data-theme="dark" data-layers="2,3,1,4" data-stack-embed="true" href="https://embed.stackshare.io/stacks/embed/0f1ad8b7ddbe84c1939681ac4579a5"/></a><script async src="https://cdn1.stackshare.io/javascripts/client-code.js" charset="utf-8"></script>

# About
Doob is a new Discord bot coded by mmatt#0001.

## Can I use Doob right now?
Yes, but I must warn you there are not many features, and is in mid development.

## What commands are there?
Right now you can do -help and get all of the commands.
I will also tell you right here :)

Ban - Usage | -ban {-b} *@username* [reason]. Bans the member specified, permanently. **Requires the Ban Member Permission**.

ChangePrefix - Usage | -prefix { -changeprefix} *prefix*. Changes the prefix for this server. **Requires the Administrator Permission**.

Clear - Usage | -clear {-p,c, or purge} [amount]. Clears amount of messages specified.  *Default amount = 5* **Requires the Manage Messages Permission.** 

Dev - Usage | -dev {-developer, or devteam} Shows the development team of Doob bot.

Help - Usage | -help { -commands} Shows a link to this page.    

Info - Usage | -info {-botinfo} Gives basic info about Doob.

Invite - Usage | -invite Gives an invite link to the bot.

Kick - Usage | -kick {-k} *member* [reason]. Kicks the member specified. **Requires the Kick Members Permission.**

Ping - Usage | -ping. Shows the bot's latency between the Discord servers.

Profile - Usage | -profile {-userinfo} *Either mention the user or say their id* Gives the user some info about the user given.

SoftClear - Usage | -softclear {-sp, sc, or softpurge} Same thing as -clear, except the default value is 2.

Support - Usage | -support. Gives support server + dev's username.    

Unban - Usage | -unban *Username*. Unbans a specific user. **Requires Ban Members Permission**    

VCShare - Usage | -vcshare {-vc, -screenshare, -ss}. Gives link to screenshare in a voice channel. Must be in a voice channel to work. [FAN FAVORITE]  

Warn - Usage | -warn {-w} *member* [reason]. Warns the user with a reason if provided. **Requires the Manage Messages Permission.**

# Stay up-to-date at https://www.notion.so/Commands-96fb0c52dee34ce6a45f42655db1f8d4

## Docs
https://www.notion.so/Doob-Docs-7ec5b5ed1a48416b82c98ada9ed5319a

## Want our stack? Here you go!

I don't know why you would ever want this, but I decided to make it because im bored.
https://stackshare.io/mmatt/doob

## Issues or want to improve some code?
Submit an [Issue](https://gitlab.com/mmatt625/doob/issues) or a [Pull request](https://gitlab.com/mmatt625/doob/-/merge_requests).
